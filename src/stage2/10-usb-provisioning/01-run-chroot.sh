#!/usr/bin/env bash

# Enable usb-provisioning service via systemd.
systemctl enable usb-provisioning

# Enable I2C.
sed -i -e "s/#dtparam=i2c_arm=on/dtparam=i2c_arm=on/" /boot/config.txt

# Enable SPI.
sed -i -e "s/#dtparam=spi=on/dtparam=spi=on/" /boot/config.txt

# Disable audio.
sed -i -e "s/dtparam=audio=on/#dtparam=audio=on/" /boot/config.txt

# Enable UART.
echo "enable_uart=1" >> /boot/config.txt

# Disable Bluetooth to prevent collision with UART.
systemctl disable hciuart

# Start X-Server.
echo "start_x=1" >> /boot/config.txt

# Configure GPU graphics memory.
echo "gpu_mem=128" >> /boot/config.txt

# Disable disable serial console.
sed -i -e "s/console=ttyAMA0,[0-9]\+ //" /boot/cmdline.txt
sed -i -e "s/console=serial0,[0-9]\+ //" /boot/cmdline.txt

# Load I2C kernel module.
echo "i2c-dev" >> /etc/modules

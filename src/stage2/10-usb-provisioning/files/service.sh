#!/usr/bin/env bash

# Color variables.
default='\e[00;0m'
red='\e[00;31m'
green='\e[00;32m'
magenta='\e[00;35m'

# Color shortcuts.
d=${default}
r=${red}
g=${green}
m=${magenta}

# Message indicators.
info="${g}info:${d}   "
error="${r}error:${d}  "

# Display error and exit program in case a command failed.
# First parameter is the return code of a program,
# which can be obtained by $? and the second parameter
# is a message in case the return code is non-zero.
# Usage: check_error $? "Something went wrong."
check_error() {
  if [ ${1} -ne 0 ]; then
    echo -e "${error}${2}"
    exit 1
  fi
}

# Check if internet connection is available.
wget -q --spider google.com
if [ $? -ne 0 ]; then
  # Configure on-board LED for custom control and enable it.
  echo "none" > /sys/class/leds/led0/trigger
  echo "1" > /sys/class/leds/led0/brightness

  echo -e "${info}Please insert a USB drive."

  # Wait for USB drive to be inserted.
  usbDevice="/dev/sda1"
  while [ ! -e "${usbDevice}" ]; do
    echo "0" > /sys/class/leds/led0/brightness
    sleep .5
    echo "1" > /sys/class/leds/led0/brightness
    sleep .5
  done

  # Create mount directory if necessary.
  mountDir="/mnt/usb"
  if [ ! -d "${mountDir}" ]; then
    echo -e "${info}Creating mount directory: ${m}${mountDir}${d}"
    mkdir -p "${mountDir}"
  fi

  # Mount USB.
  echo -e "${info}Mounting USB drive..."
  mount "${usbDevice}" "${mountDir}"

  # Check if installation configuration exists.
  installConfig="install.ini"
  if [ ! -f "${mountDir}/${installConfig}" ]; then
    # Display error if file is not found.
    echo -e "${error}Configuration file is missing: ${m}install.ini${d}"
    echo -e "${error}Please put an ${m}${installConfig}${d} onto the root of the drive."

    echo -e "${info}Unmounting USB drive..."
    sudo umount "${mountDir}"

    # Flash LED quickly to indicate error.
    for i in {0..2}; do
      echo "0" > /sys/class/leds/led0/brightness
      sleep 1
      echo "1" > /sys/class/leds/led0/brightness
      sleep .5
    done

    # Wait for USB to be removed.
    echo -e "${info}Please remove the USB drive."
    while [ -e "${usbDevice}" ]; do
      echo "0" > /sys/class/leds/led0/brightness
      sleep .5
      echo "1" > /sys/class/leds/led0/brightness
      sleep .5
    done

    # Exit unsuccessfully
    exit 1

  else
    # Permanently enable LED to indicate busy state.
    echo "1" > /sys/class/leds/led0/brightness
    echo -e "${info}Extracting configuration from file: ${m}install.ini${d}"

    # Extract variables from install.ini.
    ssid=$(grep "SSID=" ${mountDir}/${installConfig} | sed -e "s/SSID=//g" | tr -d '\r')
    psk=$(grep "PSK=" ${mountDir}/${installConfig} | sed -e "s/PSK=//g" | tr -d '\r')
    keyManagement=$(grep "KEY_MGMT=" ${mountDir}/${installConfig} | sed -e "s/KEY_MGMT=//g" | tr -d '\r')
    scriptUrl=$(grep "SCRIPT_URL=" ${mountDir}/${installConfig} | sed -e "s/SCRIPT_URL=//g" | tr -d '\r')

    # Copy environment file to temporary directory.
    tempDir="/tmp/usb-provisioning"
    mkdir -p "${tempDir}"
    if [ -f "${mountDir}/environment.ini" ]; then
      cat "${mountDir}/environment.ini" | tr -d '\r' > "${tempDir}/environment.ini"
    else
      touch "${tempDir}/environment.ini"
    fi

    # Copy Wi-Fi configuration file template.
    echo -e "${info}Generating Wi-Fi configuration..."
    cp -rf /opt/usb-provisioning/wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf

    # Encrypt PSK.
    pskEncrypted=$(wpa_passphrase "${ssid}" "${psk}" | grep "\spsk" | sed -e "s/psk=//g" /dev/stdin  | tr -d '[:space:]')

    # Interpolate template.
    sed -i "s/{{SSID}}/${ssid}/g" /etc/wpa_supplicant/wpa_supplicant.conf
    sed -i "s/{{PSK}}/${pskEncrypted}/g" /etc/wpa_supplicant/wpa_supplicant.conf
    sed -i "s/{{KEY_MGMT}}/${keyManagement}/g" /etc/wpa_supplicant/wpa_supplicant.conf

    # Unblock WiFi.
    rfkill unblock wifi

    # Unmount USB drive from filesystem.
    echo -e "${info}Unmounting USB drive..."
    umount "${mountDir}"

    # Disable LED to indicate that USB can be removed.
    echo "0" > /sys/class/leds/led0/brightness
    echo -e "${info}Please remove the USB drive."

    # Wait for USB drive to be removed.
    while [ -e "${usbDevice}" ]; do
      echo "0" > /sys/class/leds/led0/brightness
      sleep .5
      echo "1" > /sys/class/leds/led0/brightness
      sleep .5
    done

    # Disable Wi-Fi power saving.
    echo -e "${info}Disabling Wi-Fi power saving..."
    iw dev wlan0 set power_save off

    # Restart systemd and DHCP client daemon.
    echo -e "${info}Reloading systemd daemon..."
    systemctl daemon-reload
    echo -e "${info}Restarting dhcpcd daemon..."
    systemctl restart dhcpcd

    # Check for internet connection.
    wget -q --spider google.com
    if [[ ! ($? -eq 0) ]]; then
      echo -e "${error}Network connection could not be established."
      echo -e "${error}Please make sure you entered the correct SSID and password."
      exit 1
    fi

    # Download installation script.
    wget -nv -O "${tempDir}/install.sh" "${scriptUrl}"
    check_error $? "Downloading provisioning script failed."
    chmod +x "${tempDir}/install.sh"
    check_error $? "Making provisioning script executable failed."

    # Run and repeat script while it is not succeeding.
    /bin/bash "${tempDir}/install.sh"
    while [ $? -ne 0 ]; do
      /bin/bash "${tempDir}/install.sh"
    done

    # Disable usb-provisioning service.
    echo -e "${info}Permanently disabling USB provisioning service..."
    echo "0" > /sys/class/leds/led0/brightness
    systemctl disable usb-provisioning
    systemctl stop usb-provisioning
    exit 0

  fi
fi

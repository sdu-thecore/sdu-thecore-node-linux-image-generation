#!/usr/bin/env bash

# Create application directory for provisioning service and copy service definition.
install -d -m 744 "${ROOTFS_DIR}/opt/usb-provisioning"
install -m 744 files/service.sh	"${ROOTFS_DIR}/opt/usb-provisioning/service.sh"

# Create systemd service.
install -m 744 files/usb-provisioning.service "${ROOTFS_DIR}/lib/systemd/system/usb-provisioning.service"

# Copy Wi-Fi configuration file template.
install -m 744 files/wpa_supplicant.conf "${ROOTFS_DIR}/opt/usb-provisioning/wpa_supplicant.conf"

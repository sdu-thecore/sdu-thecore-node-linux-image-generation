# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Build of `raspbian-lite`-based image in CI via debian stretch
- [MIT License](https://mit-license.org/)
- CI with `.gitlab-ci.yml`
